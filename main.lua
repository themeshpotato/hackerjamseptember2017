local direction = 0 -- Up 0 Down 1 Left 2 Right 3
local snake_length = 15
local snake_size = 8
local snake_positions
local current_time = 0.0
local time_for_next = 0.03
local grid
local field_size = 8
local field_connections = {}
local field_connection_count = 0
local shooting = false
local shooting_direction
local shot_position = {}
local current_shot_time = 0.0
local time_for_next_shot_increment = 0.01
local pending_field = -1

local fields
local field_count = 0

local explosion_sound

function buildSnake()
    snake_positions = {}
    x_offset = snake_size
    for i = 0, snake_length - 1 do
        snake_positions[i] = {}
        snake_positions[i][0] = x_offset
        snake_positions[i][1] = 0
        x_offset = x_offset + 1
    end
end

function buildGrid()
    grid = {}
    for i = 0, 100 do
        grid[i] = {}
        for j = 0, 100 do
            grid[i][j] = 0
        end
    end
end

function spawnNetworkFields()
    spawn_position = {}
    spawn_position[0] = love.math.random(0, 99 - field_size)
    spawn_position[1] = love.math.random(0, 99 - field_size)
    fields[field_count] = {}
    fields[field_count][0] = spawn_position[0]
    fields[field_count][1] = spawn_position[1]
    field_count = field_count + 1

    spawn_position2 = {}
    spawn_position2[0] = love.math.random(0, 99 - field_size)
    spawn_position2[1] = love.math.random(0, 99 - field_size)
    fields[field_count] = {}
    fields[field_count][0] = spawn_position2[0]
    fields[field_count][1] = spawn_position2[1]
    field_count = field_count + 1
end

function love.load()
    love.window.setMode(800, 800, {resizable = false, vsync = false})
    explosion_sound = love.audio.newSource("assets/explosion.wav", "static")
    direction = 1
    buildSnake()
    buildGrid()
    fields = {}
    spawnNetworkFields()
end

function connectFields(connection_index, first_field, second_field)
    current_position = {}
    current_position[0] = fields[first_field][0]
    current_position[1] = fields[first_field][1]
    
    x_distance = fields[second_field][0] - fields[first_field][0]
    if x_distance > 0 then
        for x = 0, math.abs(x_distance) - 1 do
            grid[fields[first_field][0] + x][fields[first_field][1]] = connection_index
        end
        current_position[0] = fields[first_field][0] + (x_distance - 1)
    elseif x_distance < 0 then
        for x = 0, math.abs(x_distance) - 1 do
            grid[fields[first_field][0] - x][fields[first_field][1]] = connection_index
        end
        current_position[0] = fields[first_field][0] + (x_distance - 1)
    end

    y_distance = fields[second_field][1] - fields[first_field][1]
    if y_distance > 0 then
        for y = 0, math.abs(y_distance) - 1 do
            grid[current_position[0]][fields[first_field][1] + y] = connection_index
        end
    elseif y_distance < 0 then
        for y = 0, math.abs(y_distance) - 1 do
            grid[current_position[0]][fields[first_field][1] - y] = connection_index
        end
    end
end

function love.update(delta)
    if shooting then
        current_shot_time = current_shot_time + delta
        if current_shot_time >= time_for_next_shot_increment then
            current_shot_time = 0.0
            if shooting_direction == 0 then
                shot_position[1] = shot_position[1] - 1
            elseif shooting_direction == 1 then
                shot_position[1] = shot_position[1] + 1
            elseif shooting_direction == 2 then
                shot_position[0] = shot_position[0] - 1
            elseif shooting_direction == 3 then
                shot_position[0] = shot_position[0] + 1
            end
            for field_index = 0, field_count - 1 do
                for x = 0, field_size - 1 do
                    for y = 0, field_size - 1 do
                        if shot_position[0] == fields[field_index][0] + x and shot_position[1] == fields[field_index][1] + y then
                            field_connections[field_connection_count] = {}
                            field_connections[field_connection_count][0] = pending_field
                            field_connections[field_connection_count][1] = field_index
                            connectFields(field_connection_count + 1, pending_field, field_index)
                            shooting = false
                            pending_field = -1
                        end
                    end
                end
            end
        end
    end

    current_time = current_time + delta

    if current_time >= time_for_next then
        for i = 1, snake_length - 1 do
            index = snake_length - i
            snake_positions[index][0] = snake_positions[index - 1][0]
            snake_positions[index][1] = snake_positions[index - 1][1]
        end
        current_time = 0.0
        if direction == 0 then
            snake_positions[0][1] = snake_positions[0][1] - 1
        elseif direction == 1 then
            snake_positions[0][1] = snake_positions[0][1] + 1
        elseif direction == 2 then
            snake_positions[0][0] = snake_positions[0][0] - 1
        elseif direction == 3 then
            snake_positions[0][0] = snake_positions[0][0] + 1
        end
        
        if snake_positions[0][0] == -1 then
            snake_positions[0][0] = 99
        elseif snake_positions[0][1] == -1 then
            snake_positions[0][1] = 99
        elseif snake_positions[0][0] == 100 then
            snake_positions[0][0] = 0
        elseif snake_positions[0][1] == 100 then
            snake_positions[0][1] = 0
        end

        for i = 0, snake_length - 1 do
            value = grid[snake_positions[i][0]][snake_positions[i][1]]
            if value > 0 then
                field_connections[value - 1][0] = -1
                field_connections[value - 1][1] = -1
            end
        end
    end
end

function love.keypressed(key)
    if key == 'space' then
        for field_index = 0, field_count - 1 do
            for x = 0, field_size - 1 do
                for y = 0, field_size - 1 do
                    if snake_positions[0][0] == fields[field_index][0] + x and snake_positions[0][1] == fields[field_index][1] + y then
                        pending_field = field_index
                    end
                end
            end
        end
    end

    if key == 'w' or key == 'up' and direction ~= 1 then
        direction = 0
    elseif key == 's' or key == 'down' and direction ~= 0 then
        direction = 1
    elseif key == 'a' or key == 'left' and direction ~= 3 then
        direction = 2
    elseif key == 'd' or key == 'right' and direction ~= 2 then
        direction = 3
    end
end

function love.keyreleased(key)
    if key == 'space' and pending_field ~= -1 then
        shooting_direction = direction
        shooting = true
        shot_position[0] = snake_positions[0][0]
        shot_position[1] = snake_positions[0][1]
    end
end

function drawFieldConnections()
    for x = 0, 100 - 1 do
        for y = 0, 100 - 1 do
            if grid[x][y] > 0 then
                if field_connections[grid[x][y]] ~= -1 then
                    love.graphics.rectangle("fill", x * snake_size, y * snake_size, snake_size, snake_size)
                end
            end
        end
    end
end

function drawFields()
    love.graphics.setColor(255, 255, 255)
    drawFieldConnections()
    love.graphics.setColor(255, 255, 20)
    for i = 0, field_count - 1 do
        field_position = fields[i]
        for x = 0, field_size - 1 do
            for y = 0, field_size - 1 do
                love.graphics.rectangle("fill", field_position[0] * snake_size + x * snake_size, field_position[1] * snake_size + y * snake_size, snake_size, snake_size)
            end
        end
    end

    if shooting then
        love.graphics.setColor(255, 255, 255)
        love.graphics.rectangle("fill", shot_position[0] * snake_size, shot_position[1] * snake_size, snake_size, snake_size)
    end
end

function love.draw()
    drawFields()
    for i = 0, snake_length - 1 do
        if i == 0 then
            love.graphics.setColor(37, 140, 20)
        else
            love.graphics.setColor(57, 255, 20)
        end
        love.graphics.rectangle("fill", snake_positions[i][0] * snake_size, snake_positions[i][1] * snake_size, snake_size, snake_size)
    end
end